#!/bin/bash

# This script gives information about a file

read -p "Provide a path to the file: " filepath

echo "Properties for $filepath:"

if [ -f $filepath ]; then
	echo "Size is $(ls -lh $filepath | awk '{ print $(NF-4) }')"
	echo "Type is$(file $filepath | cut -d":" -f2 - )"
	echo "Inode number is $(ls -i $filepath | cut -d" " -f1 - )"
	echo "$(df -h $filepath | grep -v Mounted | awk '{ print "On", $1", "\
		"which is mounted as the", $6,"partition."}')"
else
	echo "File does not exist."
fi
