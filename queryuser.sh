#!/bin/bash

# This script asks user for the age, and if it is equal to or higher than 16
# print a message that user is allowed to drink alcohol. If the user's age
# is below 16, prints a message telling the user how many years he or she needs
# to wait before legally being allowed to drink.

echo -n "Enter the number of years you have: "
read age

if [ "$age" -ge "16" ]; then
	echo "You are allowed to drink alcohol!"
	if [ "$age" -gt "18" ]; then
		echo "An user of about your age has drink around"\
		       "$((100*($age-16))) liters until now."
        fi
else
	echo "You are not allowed to drink alcohol! You will need to wait"\
		"$((16-$age)) years before you may drink alcohol!"
fi
