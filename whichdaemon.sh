#!/bin/bash

if ps -aux | grep httpd | grep -v grep 1>/dev/null ; then
	echo "This machine is running a web server."
elif ps -aux | grep init | grep -v grep 1>/dev/null ; then
	echo "This machine uses systemV"
fi
