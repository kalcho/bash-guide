#!/bin/bash

# Calculate rectangle surface

# Get rectangle sides from command-line arguments
a=$1
b=$2

echo "Calculating rectangle surface..."
S=$[ a*b ] # rectangle surface calculation
echo "Surface of rectangle: $S"
