#!/bin/bash

# This script gives information about a file.

filename=$1

echo "Properties for $filename:"
pwd

if [[ -f $filename ]]; then
	echo "Size is $(ls -lh $filename | awk '{ print $(NF-4) }')"
	echo "Type is $(file $filename | cut -d":" -f2 -)"
	echo "Inode number is $(ls -i $filename | cut -d" " -f1 -)"
	echo "$(df -h $filename | grep -v Mounted | awk '{ print "On",$1 ","\
		" which is mounted as the", $6, "partition."}')"
else
	echo "File does not exist."
fi
