#!/bin/bash

# This script provides wisdom
# You can now exit in a decent way.

fortune=/usr/games/fortune

i=0
while [ "$i" -lt 3 ]; do
	((i++))
	echo $i
	echo "On which topic do you want advice?"
	echo "1. politics"
	echo "2. startrek"
	echo "3. kernelnewbies"
	echo "4. sports"
	echo "5. bofh-excuses"
	echo "6. magic"
	echo "7. love"
	echo "8. literature"
	echo "9. drugs"
	echo "10. education"
	echo

	echo -n "Enter your choice, or 0 for exit: "
	read choice
	echo

	case $choice in
		1)
			$fortune politics
			;;
		2)
			$fortune startrek
			;;
		3)
			$fortune kernelnewbies
			;;
		4)
			echo "Sports are a waste of time, energy and money."
			echo "Go back to your keyboard."
			echo -e "\t\t\t\t -- \"Unhealthy is my middle name\" Soggie."
			;;
		5)
			$fortune bofh-excuses
			;;
		6)
			$fortune magic
			;;
		7)
			$fortune love
			;;
		8)
			$fortune literature
			;;
		9)
			$fortune drugs
			;;
		10)
			$fortune education
			;;
		0)
			echo "OK, see you!"
			break
			;;
		*)
			echo "That is not a valid choice, try a number from 0 to 10."
			;;
	esac
done
