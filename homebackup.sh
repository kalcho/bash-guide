#!/bin/bash
#: Title	: backup_home.sh
#: Date		: 2018-08-28
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Backup user's home directory. Use level 0 and level 1 backups
#:+		: depending on last available type and its age. If there is no
#:+		: full backup (level 0) create a full backup. If full backup
#:+		: already exist, and its not older than 7 days, then create
#:+		: incremental backup, else create full backup. At the end
#:+		: compress the backup and show compressed backup size.
#: Options	: None


backupsPath="/var/backups"

## Test for number of arguments. Script should run without arguments.
if [ $# -gt 0 ]; then
	echo "No argument needed!"
	echo "USAGE: ${0##*/}"
	exit 1
fi


## Determine whether the backups directory has enough free space
#+ to hold the backup.
echo "Checking disk space... Please wait, this can take a while, depending on"\
	"disk space usage"
#spaceNeeded=$(($(tar -c $HOME 2>&1 | wc -c)/1024))  ## DEBUG: This was taking
						    ##+to long, and was
						    ##+optimized with next
						    ##+line.
spaceNeeded=$(du -s -B M $HOME | awk '{ print $1 }' | sed 's/M$//' )
spaceAvailable=$(df -B M $backupsPath | grep -v Filesystem | awk '{ print $4 }' | sed 's/M$//')
## DEBUG: set -o xtrace
if [ $spaceNeeded -ge $spaceAvailable ]; then
       echo "Not enough free space in $backupsPath!"
       echo "Clean garbage in $backupsPath to free some space."
       exit 2
fi
## DEBUG: set +o xtrace


## Ask the user whether full or incremental backup is wanted. If a user does
#+ not have full backup yet print a message that full backup will be taken.
#+ In case of incremental backup only do this if the full backup is not older
#+ than a week.
read -p "Full or incremental backup (f-Full,i-Incremental)? " -n 1 backupType
if [ $backupType == "f" ]; then
	echo "Creating full backup of your $HOME directory..."
	fullBackupName="backup-full-$(date +%Y%m%d)"
	tar -cpvf "$backupsPath/$fullBackupName.tar" \
		--listed-incremental="$backupsPath/$fullBackupName.snar" \
		$HOME &>/dev/null
	echo "Full backup created in $backupsPath/ directory!"
	## Compress the backup and calculate its size after compression
	echo "Started compressing backup..."
	gzip "$backupsPath/$fullBackupName.tar"
	size=$(bc<<<"scale=2; $(wc -c < \
		"$backupsPath/$fullBackupName.tar.gz")/1024")	
	echo "Backup compressed, taking ${size}MBs"

elif [ $backupType == "i" ]; then
	## List existing backups, but display it without user and group owners
	#+ and with modification time in seconds since epoch, and sort by time
	#+ descending.
	fullBackup=$(ls -gG --time-style=+%s -t $backupsPath | 
		grep ".*backup-full-[0-9]\{8\}\.tar.gz")
	fullBackupAge=$(echo "$fullBackup" | awk '{ print $4 }' | head -n 1)
	#echo "DEBUG: $fullBackupAge"
	
	## DEBUG: set -o xtrace
	if [ -z "$fullBackup" ]; then
		echo "Full backup of your $HOME does not exist! Full backup of"\
			"your $HOME directory would be taken"
		echo "Taking full backup of your $HOME directory..."
		fullBackupName="backup-full-$(date +%Y%m%d)"
		tar -cpvf "$backupsPath/$fullBackupName.tar" \
		       --listed-incremental="$backupsPath/$fullBackupName.snar" \
		       $HOME &>/dev/null
		echo "Full backup created in $backupsPath/ directory!"
		## Compress the backup and calculate its size after compression
		echo "Started compressing backup..."              	
                gzip -f "$backupsPath/$fullBackupName.tar"
                size=$(bc<<<"scale=2; $(wc -c < \
			"$backupsPath/$fullBackupName.tar.gz")/1024")
		echo "Backup compressed, taking ${size}MBs"
	elif [ $(( $(date +%s) - $fullBackupAge )) -le 604800 ]; then
		echo "Taking incremental backup of your $HOME directory..."
		incrementalBackupName="backup-incr-$(date +%Y%m%d)"
		fullBackupName=$fullBackup | awk '{ print $5 }' | head -n 1

		## Preserve original .snar file to create just level 1 backups
		cp "$backupsPath/$fullBackupName.snar" "/tmp/$fullBackupName.snar.$PID.tmp"
		tar -cpvf "$backupsPath/$incrementalBackupName.tar" \
			--listed-incremental="$backupsPath/${fullBackupName%\.tar}.snar" \
			$HOME &>/dev/null
		## Bring back original .snar file for use for next incremental
		#+ level 1 backup
		mv -f "/tmp/$fullBackupName.snar.$PID.tmp" \
			"$backupsPath/$fullBackupName.snar"
		echo "Incremental backup created in $backupsPath/ directory!"
		## Compress the backup and calculate its size after compression
		echo "Started compressing backup..."              	
                gzip -f "$backupsPath/$incrementalBackupName.tar"
                size=$(bc<<<"scale=2; $(wc -c <	"$backupsPath/$incrementalBackupName.tar.gz")/1024")
		echo "Backup compressed, taking ${size}MBs"

	else
		echo "Your full backup is older than 7 days! Creating full"\
		       "backup of your $HOME directory..."
		fullBackupName="backup-full-$(date +%Y%m%d)"
                tar -cpvf "$backupsPath/$fullBackupName.tar" \
			--listed-incremental="$backupsPath/$fullBackupName.snar" \
			$HOME &>/dev/null
		echo "Full backup created in $backupsPath/ directory!"
		## Compress the backup and calculate its size after compression
		echo "Started compressing backup..."              	
                gzip -f "$backupsPath/$fullBackupName.tar"
                size=$(bc<<<"scale=2; $(wc -c < "$backupsPath/$fullBackupName.tar.gz")/1024")
		echo "Backup compressed, taking ${size}MBs"
	fi
	## DEBUG: set +o xtrace

else
	echo "USAGE: You need to provide 'f' - for full,"\
		"or 'i' - for incremental backup."
	exit 3
fi
