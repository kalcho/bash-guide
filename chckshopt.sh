#!/bin/bash

# This script will check if the noclobber option is set.
# noclobber -> option disables overwriting the files at redirection.

echo "This script checks if shell option noclobber has been turned on."
echo "Checking..."
if [ -o noclobber ]
then
	echo "Your files are protected against accidental overwritting \
	       using redirection."
else
	echo "noclobber option is turned off."
fi
echo
echo "...done."
