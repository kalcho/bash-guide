#!/bin/bash

# This script converts all file names containing upper case characters into
# file names

list="$(ls)"
for name in "$list"; do
	if [[ "$name" != *[[:upper:]]* ]]; then
		continue
	fi

	orig="$name"
	new=`echo $name | tr 'A-Z' 'a-z'`
	
	if [ -f "$new" ]; then
		echo "File $new alreday exist! No change will occur for $orig."
	else
		mv "$orig" "$new"
		echo "new name for $orig is $new"
	fi
done
